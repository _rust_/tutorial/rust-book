# RUST BOOK

## Учебник по яызку RUST

### [Ссылка на источник](https://doc.rust-lang.ru/book/title-page.html)

## Цели прохождение руководство по RUST:

1. Выучить синтаксис
2. Понять концепцию языка
3. Понять как работает строгая тепизация
4. Разобраться как работает память в программах
5. Свободное использование языка

## Список пройденного материала с примерами

1. [С чего начать](./1_getting_started/README.md)

2. [Программируем игру Угадайка](./2_prgramming_a_guessing_game/README.md)

3. [Общие концепции программирования](./3_common_programming_concepts/README.md)

4. [Понимания владения](./4_understanding_ownership/README.md)
