use rand::Rng; // Библиотке для рандома
use std::cmp::Ordering; // Является перечислением и имеет варианты Less, Greater и Equal
use std::io; // Библиотека ввода/вывода io

fn main() {
    let secret_number: u32 = rand::thread_rng().gen_range(1..101); // Рандомное число от 1 до 101
    println!("Угадай число!");
    // Бесконечный цикл
    loop {
        println!("Пожалуйста, введите ваше предположение.");

        // Переменная для хранения пользовательского ввода
        // Используется модификатор mut для будущего изменения
        let mut guess = String::new();

        io::stdin() // Вызывается функция для стандартного ввода в терминале
            .read_line(&mut guess) // Записываем в переменную значение ввода
            .expect("Не удалось прочитать строку"); // Обработка потенциального сбоя с помощью типа Result

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Введите пожалуйста число");
                continue; // Продолжить цикл
            }
        };

        println!("Ваше число: {}", guess);

        // Сравнение двух чисел
        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Слишком маленькое число!"),
            Ordering::Greater => println!("Слишком большое число!"),
            Ordering::Equal => {
                println!("Вы выйграли!");
                break; // Выйти из цикла
            }
        };
    }
}
