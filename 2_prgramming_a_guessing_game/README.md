# Программируем игру Угадайка

1. Настройка нового проекта

```rust
cargo new guessing_game
```

2. Получение пользовательского ввода

```rust
io::stdin()
    .read_line(&mut guess);
```

3. Обработка потенциального сбоя с помощью типа Result

```rust
io::stdin()
    .read_line(&mut guess)
    .expect("Не удалось прочитать строку");
```

4. Напечатать значений с помощью заполнителей println!

```rust
println!("Ваше число: {}", guess);
```

5. Генерация секретного числа

```rust
let secret_number: u32 = rand::thread_rng().gen_range(1..101);
```

6. Сравнение догадки с секретным числом

```rust
match guess.cmp(&secret_number) {
    Ordering::Less => println!("Слишком маленькое число!"),
    Ordering::Greater => println!("Слишком большое число!"),
    Ordering::Equal => {
        println!("Вы выйграли!");
        break;
    }
};
```

7. Возможность нескольких догадок с помощью циклов

```rust
loop {
    println!("Please input your guess.");

    // --snip--
}
```

8. Выход после правильной догадки

```rust
match guess.cmp(&secret_number) {
    Ordering::Less => println!("Слишком маленькое число!"),
    Ordering::Greater => println!("Слишком большое число!"),
    Ordering::Equal => {
        println!("Вы выйграли!");
        break; // <-
    }
};
```

9. Обработка недопустимого ввода

```rust
let guess: u32 = match guess.trim().parse() {
    Ok(num) => num,
    Err(_) => {
        println!("Введите пожалуйста число");
        continue;
    }
};
```

## [Ссылка](./guessing_game/src/main.rs) на программу

## [Ссылка](https://doc.rust-lang.ru/book/ch02-00-guessing-game-tutorial.html) на источник
