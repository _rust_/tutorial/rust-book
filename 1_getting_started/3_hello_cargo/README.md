# Привет, Cargo!

## Цель

1. Создание проекта с помощью Cargo

```rust
cargo new hello_cargo
cd hello_cargo
```

2. Сборка и запуск Cargo проекта

```rust
cargo build // Сборка проекта
cargo run // Запуск проекта
```
