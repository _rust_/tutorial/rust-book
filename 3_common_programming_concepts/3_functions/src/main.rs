fn main() {
    println!("Hello, world!");
    another_function(5);
    print_labeled_measurement(5, 'h');

    let x = five(); // 5
    let _x = plus_one(x); // 6
}

// Параметры функции
fn another_function(x: i32) {
    println!("The value of x is: {x}");
}

fn print_labeled_measurement(value: i32, unit_label: char) {
    println!("The measurement is: {value}{unit_label}");
}

/*
    Оператор:
    Операторы - это инструкции, которые выполняют какое-либо действие и не возвращают значение. Выражения вычисляют результирующее значение. Давайте посмотрим на несколько примеров.
    let y = 6

    Выражение:
    {
        let x = 3;
        x + 1
    }
*/

// Функции возвращающие значения
fn five() -> i32 {
    5
}
fn plus_one(x: i32) -> i32 {
    x + 1
}
