# Общие концепции программирования

1. [Переменные и понятие изменяемости](./1_variables-and-mutability/README.md)
2. [Типы данных](./2_data-types/README.md)
3. [Функции](./3_functions/README.md)
4. [Комментарии](./4_comments/README.md)
5. [Поток управления](./5_control-flow/README.md)
