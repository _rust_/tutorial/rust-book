const THREE_HOURS_IN_SECONDS: u32 = 60 * 60 * 3; // Константа

fn main() {
    let x = 5; // immutability переменная
    println!("x = {}", x);
    let mut x = 2; // Затенение + mutability переменная
    println!("x = {}", x);
    x = 6; // Изменение
    println!("x = {}", x);
    println!("THREE_HOURS_IN_SECONDS = {}", THREE_HOURS_IN_SECONDS);
}
