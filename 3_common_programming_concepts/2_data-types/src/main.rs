fn main() {
    let i8 = -1_i8; // 8-bit значения между -128 и 128.
    let i16 = -1_i16; // 16-bit
    let i32 = -1_i32; // 32-bit
    let i64 = -1_i64; // 64-bit
    let i128 = -1_i128; // 128-bit
    let isize = -1_isize; // arch

    let u8 = 1_u8; // 8-bit значения между 0 и 255.
    let u16 = 1_u16;
    let u32 = 1_u32;
    let u64 = 1_u64;
    let u128 = 1_u128;
    let usize = 1_usize;
}
