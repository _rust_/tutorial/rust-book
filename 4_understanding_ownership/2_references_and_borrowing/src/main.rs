fn main() {
    let s1 = String::from("hello");

    let len = calculate_length(&s1); //  Передаём ссылку

    println!("The length of '{}' is {}.", s1, len);

    let mut s = String::from("hello");

    // let r1 = &mut s;
    // let r2 = &mut s;
    // println!("{}, {}", r1, r2);
    // Мы не можем заимствовать s как изменяемые более одного раза в один момент.

    change(&mut s);
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}

fn calculate_length(s: &String) -> usize {
    s.len()
}
