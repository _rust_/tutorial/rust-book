#[derive(Debug)]
struct MyStruct<'a> {
    field_1: i32,
    field_2: &'a str,
    field_3: String,
}

fn main() {
    let a = 32; // стэк
    let mut b = "hello"; // стэк
    let c = String::from("String"); // Куча
                                    /*
                                        c

                                        name     | value
                                        ---------|--------     index | value
                                        ptr      | --------->   0    |   S
                                        len      | 6            1    |   t
                                        capacity | 6            2    |   r
                                                                3    |   i
                                                                4    |   n
                                                                5    |   g
                                    */
    let d = MyStruct {
        field_1: a, // Копирование значения потому что является простым типом
        field_2: b, // Ссылка на значение в переменной b, при условии(<'a>) что она проживёт дольше структуры "MyStruct"
        field_3: c, // "Перемещение" cсылки в field_3
                    // !!! доступ к cсылки "с" пропал
                    // !!! НЕ "поверхностное копирование"(shallow copy), а именно "Перемещение"(move)
    }; // Куча

    println!("a: {}, b: {}, d: {:?}", a, b, d);
    // println!("c: {}", c); // Не сработает потому что мы передали ссылку в field_3

    /*
        Освобождение памяти - drop();
        drop(d);
        !!! drop(c) не происходит потому что rust считает эту переменная уже не используемой, потому что она "переместилась" field_3
        !!! Это делается для избежания ошибки "двойного освобождения" (double free)
        drop(b);
        drop(a);
    */
}
