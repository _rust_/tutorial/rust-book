# Понимания владения

1. [Что такое владение](./1_what_is_ownership/README.md)
2. [Ссылки и заимствование](./2_references_and_borrowing/README.md)
3. [Тип срезы](./3_slices/README.md)
